/* eslint-disable no-console */
import { exec, PromiseWithChild } from 'node:child_process';
import { PathLike, promises as fs, Stats } from 'node:fs';
import * as Path from 'node:path';
import util from 'node:util';
import { Project } from './models';

export const execAsync: (command: string) => PromiseWithChild<{
    stdout: string;
    stderr: string;
}> = util.promisify(exec);

export interface ExecOutput {
    stdout: string;
    stderr: string;
}

export interface GitCheck {
    isGit: boolean;
    dirty: boolean;
    upToDate: boolean;
}

export async function pathExists(path: PathLike): Promise<boolean> {
    try {
        await fs.access(path);
        return true;
    } catch {
        return false;
    }
}

export async function isFolder(path: PathLike): Promise<boolean> {
    try {
        const stat: Stats = await fs.stat(path);
        return stat.isDirectory();
    } catch {
        return false;
    }
}

export async function checkGitRepo(path: string): Promise<GitCheck> {
    const checkRes: GitCheck = { isGit: false, dirty: false, upToDate: false };
    try {
        try {
            const { stdout } = await execAsync(`git -C ${path} status --short`);
            checkRes.isGit = true;
            checkRes.dirty = Boolean(stdout);
        } catch { }
        const { stdout } = await execAsync(`git -C ${path} diff origin/HEAD --name-only`);
        checkRes.upToDate = !stdout;
    } catch { }
    return checkRes;
}

export async function folderFilled(path: PathLike): Promise<boolean> {
    try {
        const files: Array<string> = await fs.readdir(path);
        return files.length != 0;
    } catch {
        return false;
    }
}

export async function cloneProject(project: Project, workDir: string): Promise<void> {
    const projectName: string = project.name_with_namespace.replace(/ /ug, '');
    const targetProjectPath: string = Path.join(workDir, project.path_with_namespace);
    const isFolderFilled: boolean = await folderFilled(targetProjectPath);
    if (isFolderFilled) {
        console.log(`skipped since folder: "${targetProjectPath}" is not empty`);
        return;
    }
    try {
        console.log(`cloning project "${projectName}" into "${targetProjectPath}" ...`);
        await execAsync(`git clone ${project.http_url_to_repo} ${targetProjectPath}`);
        console.log('done');
    } catch (error: unknown) {
        console.log(`error occured when cloning project: "${projectName}"`, error);
    }
}

export async function updateProject(project: Project, workDir: string): Promise<void> {
    const projectName: string = project.name_with_namespace.replace(/ /ug, '');
    const targetProjectPath: string = Path.join(workDir, project.path_with_namespace);
    const isFolderFilled: boolean = await folderFilled(targetProjectPath);
    if (!isFolderFilled) {
        console.log(`skipped since folder: "${targetProjectPath}" is empty`);
        return;
    }
    try {
        console.log(`update project "${projectName}" at "${targetProjectPath}" ...`);
        await execAsync(`git -C ${targetProjectPath} pull`);
        console.log('done');
    } catch (error: unknown) {
        console.log(`error occured when update project: "${projectName}"`, error);
    }
}

export async function cloneAll(projects: Array<Project>, workDir: string): Promise<void> {
    console.log('starting clone all projects...');
    for (let i: number = 0; i < projects.length; i++) {
        const currentProject: Project = projects[i];
        console.log(`processing project "${currentProject.name_with_namespace.replace(/ /ug, '')}" (${i + 1}/${projects.length})...`);
        await cloneProject(currentProject, workDir);
    }
}

export async function pullToUpdate(projects: Array<Project>, workDir: string): Promise<void> {
    console.log('starting update projects...');
    for (let i: number = 0; i < projects.length; i++) {
        const currentProject: Project = projects[i];
        console.log(`processing project "${currentProject.name_with_namespace.replace(/ /ug, '')}" (${i + 1}/${projects.length})...`);
        await updateProject(currentProject, workDir);
    }
}
