/* eslint-disable no-console */
import { Gitlab, Types } from '@gitbeaker/core';
import { Gitlab as GitLab } from '@gitbeaker/node';
import inquirer from 'inquirer';
import { PathLike, promises as fs } from 'node:fs';
import * as path from 'node:path';
import { Project, Repo } from './models';
import { checkGitRepo, cloneAll, GitCheck, isFolder, pathExists, pullToUpdate } from './utils';

interface ConfigData {
    accessToken: string;
    baseUrl: string;
    action?: string;
}

const configFileName: string = './.ferry.json';

const workDir: string = process.argv[2] ?? process.cwd();

const folderBlackList: Set<string> = new Set(['.git', '.vscode', 'node_modules', 'src', 'dist']);

const folderStatCount: { folder: number; gitRepo: number; matched: number; dirty: number; upToDate: number } = { folder: 0, gitRepo: 0, matched: 0, dirty: 0, upToDate: 0 };

let remoteProjects: Array<Project> = new Array<Project>();

async function reposifyFolder(folder: PathLike, repo: Repo): Promise<void> {
    const dirs: Array<string> = await fs.readdir(folder);
    for (const dir of dirs) {
        if (folderBlackList.has(dir)) { continue; }

        const fullPath: string = path.join(repo.path, dir);

        if (!await isFolder(fullPath)) { continue; }

        const checkRes: GitCheck = await checkGitRepo(fullPath);

        const matched: boolean = (Array.isArray(remoteProjects) && remoteProjects.length > 0) ? remoteProjects.findIndex(proj => fullPath.includes(proj.path_with_namespace.replace(/\//ug, '\\'))) >= 0 : false;

        const childRepo: Repo = {
            path: fullPath,
            cloned: checkRes.isGit,
            matched,
            dirty: checkRes.dirty,
            upToDate: checkRes.upToDate,
        };

        folderStatCount.folder += 1;

        if (childRepo.cloned) {
            folderStatCount.gitRepo += 1;
            if (childRepo.matched) {
                folderStatCount.matched += 1;
            }
            if (childRepo.dirty) {
                folderStatCount.dirty += 1;
            }
            if (childRepo.upToDate) {
                folderStatCount.upToDate += 1;
            }
        } else {
            await reposifyFolder(fullPath, childRepo);
        }

        if (!Array.isArray(repo.children)) {
            repo.children = new Array<Repo>();
        }
        repo.children?.push(childRepo);
    }
}

async function main(): Promise<void> {
    console.log('Welcome to Ferry 🎉');

    console.log(`Current working folder:📂 "${workDir}"`);

    await fs.stat(workDir);

    const configFilePath: string = path.join(workDir, configFileName);

    let configData: ConfigData = { accessToken: '', baseUrl: '' };

    const configExists: boolean = await pathExists(configFilePath);
    if (configExists) {
        console.log(`"${configFilePath}" file found reading and parsing it to json...`);
        const configContent: string = await fs.readFile(configFilePath, { encoding: 'utf8' });
        try {
            configData = JSON.parse(configContent) as ConfigData;
            console.log(`"${configFilePath}" file loaded with content:\n`, configData);
        } catch (error: unknown) {
            console.log(`error eccured when reading and parsing "${configFilePath}"`, error);
            return;
        }
    } else {
        console.log(`no config file found \nyou can create a "${configFileName}" to store config data with the struct:\n`, configData);
    }

    console.log(`current working folder`, workDir ?? process.cwd());

    let inputConfig: ConfigData = await inquirer.prompt<ConfigData>([{
        'type': 'input',
        'message': 'please input your GitLab URL:',
        'name': 'baseUrl',
        'default': configData.baseUrl || 'https://gitlab.com',
    }, {
        'type': 'password',
        'message': 'please input your access token:',
        'name': 'accessToken',
        'default': configData.accessToken || undefined,
        'validate': input => (input ? true : 'please DO provide an access token'),
    }]);

    inputConfig = await inquirer.prompt<ConfigData>([{
        'type': 'list',
        'message': 'please select an action:',
        'name': 'action',
        'choices': ['Check', 'Clone', 'Update'],
        'default': 'Check',
    }], inputConfig);

    configData = { ...configData, ...inputConfig };

    console.log('Current working config:\n', configData);
    console.log(`Current working folder:📂 "${workDir}"`);

    const ready: { go: boolean } = await inquirer.prompt<{ go: boolean }>([{
        type: 'confirm',
        message: 'Ready to Go?',
        name: 'go',
    }]);
    if (!ready.go) {
        return;
    }

    console.log(`Saving current config to file "${configFileName}" ...`);
    await fs.writeFile(configFileName, JSON.stringify(configData, null, 4), { encoding: 'utf8' });

    const api: Gitlab = new GitLab({
        host: configData.baseUrl,
        token: configData.accessToken,
    });

    if (configData.action == 'Check') {
        console.log('Requesting projects and groups from GitLab API...');
        remoteProjects = await api.Projects.all({ perPage: 99 });

        if (!Array.isArray(remoteProjects) || remoteProjects.length < 1) {
            console.log('No project and group returned from GitLab API');
            return;
        }

        const remoteGroups: Array<Types.GroupSchema> = await api.Groups.all({ perPage: 99 });

        console.log(`${remoteProjects.length} Projects and ${remoteGroups.length} Groups returned from GitLab API`);

        console.log('Checking working folder...');
        const rootRepo: Repo = { path: path.normalize(workDir), cloned: false, matched: false, dirty: false, upToDate: false, children: new Array<Repo>() };

        await reposifyFolder(workDir, rootRepo);

        console.log({ folderStatCount });

        await fs.writeFile(path.join(workDir, './.repostat.json'), JSON.stringify(rootRepo, null, 4), { encoding: 'utf8' });
    } else if (configData.action == 'Clone') {
        console.log('Requesting projects from GitLab API...');
        const projects: Array<Project> = await api.Projects.all({ perPage: 99 });

        if (!Array.isArray(projects) || projects.length < 1) {
            console.log('No project returned from GitLab API');
            return;
        }

        await cloneAll(projects, workDir);
    } else if (configData.action == 'Update') {
        console.log('Requesting projects from GitLab API...');
        const projects: Array<Project> = await api.Projects.all({ perPage: 99 });

        if (!Array.isArray(projects) || projects.length < 1) {
            console.log('No project returned from GitLab API');
            return;
        }

        await pullToUpdate(projects, workDir);
    }

    console.log('Goodbye 👋');
}

main().then().catch(err => console.error('😱 error occured with ferry\n', err));
