import { Types } from '@gitbeaker/core';

export type Project = Types.ProjectSchema;

export interface ConfigData {
    accessToken: string;
    baseUrl: string;
    action: string;
}

export interface Repo {
    path: string;
    cloned: boolean;
    matched: boolean;
    dirty: boolean;
    upToDate: boolean;
    children?: Array<Repo>;
}
