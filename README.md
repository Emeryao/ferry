# Ferry ⛴
> Check/Clone/Update GitLab repos with your local folder

## Usage

* Install
```shell
$ npm i -g glferry@latest
```

* Start working
```shell
$ cd your/work/folder
$ ferry
```

## Notes
* based on the awesome work of [gitbeaker](https://github.com/jdalrymple/gitbeaker)
